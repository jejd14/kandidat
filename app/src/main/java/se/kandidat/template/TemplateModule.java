package se.kandidat.template;

import com.codahale.metrics.health.HealthCheck;
import com.google.common.collect.Sets;
import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import io.dropwizard.setup.Environment;
import se.kandidat.template.health.TemplateHealthCheck;
import se.kandidat.template.resources.JerseyResource;
import se.kandidat.template.resources.TaskResource;
import se.kandidat.template.services.TaskService;
import se.kandidat.template.services.impl.TaskServiceImpl;

import java.util.Collection;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

public class TemplateModule extends AbstractModule {
    private TemplateConfiguration templateConfiguration;
    private Environment environment;

    TemplateModule(TemplateConfiguration templateConfiguration, Environment environment) {
        this.environment = checkNotNull(environment);
        this.templateConfiguration = checkNotNull(templateConfiguration);
    }

    @Override
    protected void configure() {
        // dropwizard
        bind(TemplateConfiguration.class).toInstance(templateConfiguration);
        bind(Environment.class).toInstance(environment);

        // services
        bind(TaskService.class).to(TaskServiceImpl.class);

        // health checks
        bind(HealthCheck.class).to(TemplateHealthCheck.class);

        // resources
        Multibinder<JerseyResource> resources = Multibinder.newSetBinder(binder(), JerseyResource.class);
        getResourceClasses().forEach(resource -> resources.addBinding().to(resource));
    }

    private Collection<Class<? extends JerseyResource>> getResourceClasses() {
        Set<Class<? extends JerseyResource>> classes = Sets.newLinkedHashSet();
        classes.add(TaskResource.class);

        return classes;
    }
}
