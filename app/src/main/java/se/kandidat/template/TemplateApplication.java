package se.kandidat.template;

import com.codahale.metrics.health.HealthCheck;
import io.dropwizard.Application;
import io.dropwizard.jdbi.bundles.DBIExceptionsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.flywaydb.core.Flyway;
import se.kandidat.template.resources.JerseyResource;

import javax.inject.Inject;
import java.util.Set;

public class TemplateApplication extends Application<TemplateConfiguration> {
    @Override
    public void initialize(Bootstrap<TemplateConfiguration> bootstrap) {
        bootstrap.addBundle(new TemplateGuiceBundle(this));
        bootstrap.addBundle(new DBIExceptionsBundle());
    }

    @Override
    public void run(TemplateConfiguration configuration, Environment environment) throws Exception {
        environment.getApplicationContext().setContextPath("/");
    }

    @Inject
    void setupResources(Environment environment, Set<JerseyResource> resources) {
        resources.forEach(environment.jersey()::register);
    }

    @Inject
    void setupHealthChecks(Environment environment, HealthCheck healthCheck) {
        environment.healthChecks().register(healthCheck.toString(), healthCheck);
    }

    public static void main(String[] args) throws Exception {
        new TemplateApplication().run(args);
    }
}
