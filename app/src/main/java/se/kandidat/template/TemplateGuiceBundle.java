package se.kandidat.template;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.inject.Guice;

import io.dropwizard.Application;
import io.dropwizard.ConfiguredBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class TemplateGuiceBundle implements ConfiguredBundle<TemplateConfiguration> {
    private Application<TemplateConfiguration> application;

    TemplateGuiceBundle(Application<TemplateConfiguration> application) {
        this.application = checkNotNull(application);
    }

    @Override
    public void run(TemplateConfiguration configuration, Environment environment) throws Exception {
        Guice.createInjector(getGuiceModule(configuration, environment)).injectMembers(application);
    }

    @Override
    public void initialize(Bootstrap<?> bootstrap) {
    }

    private TemplateModule getGuiceModule(TemplateConfiguration configuration, Environment environment) {
        return new TemplateModule(configuration, environment);
    }
}
