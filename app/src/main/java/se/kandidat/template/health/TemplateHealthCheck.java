package se.kandidat.template.health;

import com.codahale.metrics.health.HealthCheck;

public class TemplateHealthCheck extends HealthCheck {
    @Override
    protected Result check() throws Exception {
        return Result.healthy();
    }

    @Override
    public String toString() {
        return "TemplateHealthCheck";
    }
}
