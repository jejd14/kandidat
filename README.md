##Host setup
Must be uid 1000
###Add git remotes
* git-server	ssh://git@localhost:2022/opt/git/project.git (fetch)
* git-server	ssh://git@localhost:2022/opt/git/project.git (push)
* gitlab		ssh://git@localhost:5022/WilliamLindblom/kandidat.git (fetch)
* gitlab		ssh://git@localhost:5022/WilliamLindblom/kandidat.git (push)

push to git-server and gitlab

###Add directory for bachelor script
Take from readme :/opt/bachelor

sudo apt-get install inotify-tools -y
sudo apt-get install curl -y  

##Git-server
###Setup
The git-server compose file will setup a local docker network named “gitserver_network” that other docker containers can connect to.
 
sudo mkdir /opt/git-server
cd /repo/git-server/
docker-compose up -d
touch /opt/git-server/ssh/authorized_keys
sudo chown -R 1000:1000 /opt/git-server

###Add git repo
1. cd /opt/git-server/repos/
2. mkdir project.git
3. cd project.git
4. git init --bare

###Add ssh keys for connecting to git-server
1. mkdir /opt/git-server/keys/
2. ssh-keygen -f /opt/git-server/keys/id_rsa -N ''
3. cat /opt/git-server/keys/id_rsa.pub >> /opt/git-server/ssh/authorized_keys

###Add your public key to git-server
1. cat ~/.ssh/id_rsa.pub >> /opt/git-server/ssh/authorized_keys

##Jenkins

###Setup
1. sudo mkdir /opt/jenkins
2. cd /repo/jenkins/
3. docker-compose up -d
4. Go to http://localhost:3080 in your browser
5. docker exec -it jenkins /bin/bash
6. cat /var/jenkins_home/secrets/initialAdminPassword
7. Install suggested plugins
8. Continue as admin
9. Change admin password to something more memorable (adminadmin)

###Create pipeline
1. Click on blueocean button in jenkins gui
2. Then “Pipelines”
3. Then “New Pipeline”
4. Choose “Git” as version control
5. Specify the git url
6. Copy the ssh key
7. Add to authorized keys for git-server

###Trigger job
1. CRUMB=$(curl -s 'http://admin:adminadmin@localhost:49001/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)')
2. curl -X POST -H "$CRUMB" http://admin:adminadmin@localhost:49001/job/project/job/master/build

Change admin password to “adminadmin”

##Concourse CI
Username: concourse
Password: changeme

###Setup
1. sudo mkdir -p /opt/concourse/keys/web /opt/concourse/keys/worker
2. sudo ssh-keygen -t rsa -f /opt/concourse/keys/web/tsa_host_key -N ''
3. sudo ssh-keygen -t rsa -f /opt/concourse/keys/web/session_signing_key -N ''
4. sudo ssh-keygen -t rsa -f /opt/concourse/keys/worker/worker_key -N ''
5. sudo cp /opt/concourse/keys/worker/worker_key.pub /opt/concourse/keys/web/authorized_worker_keys
6. sudo cp /opt/concourse/keys/web/tsa_host_key.pub /opt/concourse/keys/worker
7. cd /repo/concourse
8. docker-compose up -d
9. open up localhost:
10. go through the fly-setup
11. fly login --concourse-url http://localhost:4080 --target bachelor --username concourse --password changeme

For reference: concourse-docker-setup

###Create pipeline
1. cd /repo/concourse/
2. fly -t bachelor destroy-pipeline --non-interactive --pipeline bachelor-pipeline
3. fly -t bachelor set-pipeline --non-interactive --pipeline bachelor-pipeline --config $SCRIPT_DIR/../concourse/pipeline.yml --var "private-repo-key=$(cat /opt/git-server/keys/id_rsa)"
4. fly -t bachelor unpause-pipeline --pipeline bachelor-pipeline

###Trigger pipeline
1. fly --target bachelor trigger-job --job bachelor-pipeline/ci-pipeline

##GitLab CI

###Setup
1. mkdir /opt/gitlab/
2. cd /repo/gitlab/
3. docker-compose up -d

###Register runner to project
1. docker exec -it gitlab-runner /bin/bash
2. gitlab-runner register
3. Enter “http://gitlab-web”
4. Take the registration token from Settings → CI/CD → Runner settings
5. Use container name as description
6. Tag none [enter]
7. Lock to current project [true]
8. Use docker as the executor
9. Use ubuntu:16.04 as the default docker image
10. sudo nano /opt/gitlab/runner-config/config.toml
11. Edit “concurrent” and set it to 5
	Weird behavior when it is set to 4, only uses 3…
	While 5 concurrent actually is 5 concurrent jobs
12. Add volume "/tmp/bachelor/collection_status:/tmp/bachelor/collection_status" to volumes
13. Add network_mode =  "gitlab_default" below [runner.docker] for the specific runner

###Trigger
curl --request POST "http://localhost:5080/api/v4/projects/2/trigger/pipeline?token=TRIGGER_TOKEN&ref=master"

Example
curl --request POST "http://localhost:5080/api/v4/projects/2/trigger/pipeline?token=dd601ce7852267bc225a4193afcdc1&ref=master"

###Test execution
java -Dnumber_of_tests=10 -cp .:hamcrest-core-1.3.jar:junit-4.12.jar org.junit.runner.JUnitCore TestRunner



