#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT_DIR="$SCRIPT_DIR/.."

main() {
    update_ec_branch ec1a
    update_ec_branch ec1b
    update_ec_branch ec1c
    update_ec_branch ec2a
    update_ec_branch ec2b
    update_ec_branch ec2c
    update_ec_branch ec3a
    update_ec_branch ec3b
    update_ec_branch ec3c
    git checkout master
    git push --all origin
    git push --all git-server
    git push --all gitlab
}

update_ec_branch() {
    git checkout master
    git checkout -B $1
    git pull origin $1 --no-edit
    git merge master --no-edit

    rm --force $PROJECT_DIR/Jenkinsfile
    rm --force $PROJECT_DIR/.gitlab-ci.yml
    cp $PROJECT_DIR/jenkins/Jenkinsfile.$1 $PROJECT_DIR/Jenkinsfile
    cp $PROJECT_DIR/gitlab/.gitlab-ci.yml.$1 $PROJECT_DIR/.gitlab-ci.yml

    echo $PROJECT_DIR/Jenkinsfile
    git add $PROJECT_DIR/Jenkinsfile
    git add $PROJECT_DIR/.gitlab-ci.yml

    git commit -m "Auto add Jenkinsfile and .gitlab-ci.yml for $1 by update script"
}

main