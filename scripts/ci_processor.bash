#!/bin/bash

TEMP_FILE=/tmp/bachelor/experiment_processor_temp
TAB=$'\t'

main() {
	sed s/\[\ \]\\{3\,\\}/\\t/g $1 > $TEMP_FILE

    start_time="$(head -n3 $TEMP_FILE | tail -n1 | cut -f1)"
    start_time="$(date -d $start_time '+%s%N')"
    
    process

	rm -f $TEMP_FILE
}

process() {
    while read row; do
        if [[ -z $timestamp && -n $row ]]; then
            timestamp="$(echo "$row" | cut -f1)"
            timestamp="$(date -d $timestamp '+%s%N')"
            timestamp="$(expr $timestamp - $start_time)"
        fi

        if [[ -z $row ]]; then
            if [[ -n $cpu_percentage ]]; then
                echo "$(nano_to_seconds $timestamp)$TAB$cpu_percentage$TAB$memory_percentage"
            fi
            timestamp=""
            cpu_percentage=""
            memory_percentage=""
            block_input=""
            block_output=""
        else
            sum_row "$(echo "$row" | cut -f4-)"
        fi
    done < $TEMP_FILE
}

sum_row() {
    temp="$(echo "$1" | cut -f1 | grep --only-matching [0-9\.]*)"
    cpu_percentage="$(sum $cpu_percentage $temp)"
    temp="$(echo "$1" | cut -f3 | grep --only-matching [0-9\.]*)"
    memory_percentage="$(sum $memory_percentage $temp)"
    #temp="$(dehumanize $(echo "$1" | cut -f5 | cut -d"/" -f1))"
    #block_input="$(sum $block_input $temp)"
    #temp="$(dehumanize $(echo "$1" | cut -f5 | cut -d"/" -f2))"
    #block_output="$(sum $block_output $temp)"
}

dehumanize() {
    number="$(echo "$1" | grep --only-matching [0-9\.]*)"

    case $1 in
        [0-9\.]*KiB )
            power $number 2 10
            ;;
        [0-9\.]*MiB )
            power $number 2 20        
            ;;
        [0-9\.]*GiB )
            power $number 2 30
            ;;
        [0-9\.]*TiB )
            power $number 2 40
            ;;
        [0-9\.]*kB )
            power $number 10 3
            ;;
        [0-9\.]*MB )
            power $number 10 6
            ;;
        [0-9\.]*GB )
            power $number 10 9
            ;;
        [0-9\.]*TB )
            power $number 10 12
            ;;
        [0-9\.]* )
            ;&
        [0-9\.]*B )
            echo "$number"
            ;;
    esac
}

sum() {
    awk -v OFMT='%.2f' -v first=$1 -v second=$2 'BEGIN {print first+second}'
}

power() {
    awk -v OFMT='%.0f' -v number=$1 -v base=$2 -v exponent=$3 'BEGIN {print number*base^exponent}'
}

nano_to_seconds() {
    awk -v OFMT='%.1f' -v number=$1 'BEGIN {print number / 1000000000}'
}

main $@
