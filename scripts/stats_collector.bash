#!/bin/bash

touch $1
rm --force $2

tail -f -n0 --pid=$$ $1 |
while read line;
do
echo "$(date --iso-8601=ns)   $line" >> $2;
done;
