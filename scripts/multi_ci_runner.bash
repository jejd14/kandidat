 #!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DOCKER_SCRIPT=$SCRIPT_DIR/docker_script.bash
CI_RUNNER=$SCRIPT_DIR/ci_runner.bash

test_mode=0

main() {
  parse_arguments $@

  if [[ $test_mode == 0 ]]; then
    start_docker
  fi

  start_dry_runs
  start_runs

  if [[ $test_mode == 0 ]]; then
    stop_docker
  fi
}

parse_arguments() {
  while getopts ":o:s:b:d:r:th" opt; do
    case $opt in
      s )
        case $OPTARG in
          concourse )
            ;&
          gitlab )
            ;&
          jenkins )
            ci_software=$OPTARG
            ;;
          * )
            invalid_argument_exit $opt $OPTARG
            ;;
        esac
        ;;
      b )
        case $OPTARG in
          master )
            ;&
          ec1a )
            ;&
          ec1b )
            ;&
          ec1c )
            ;&
          ec2a )
            ;&
          ec2b )
            ;&
          ec2c )
            ;&
          ec3a )
            ;&
          ec3b )
            ;&
          ec3c )
            branch=$OPTARG
            ;;
          * )
            invalid_argument_exit $opt $OPTARG
            ;;
        esac
        ;;
      d )
        number_of_dry_runs=$OPTARG
        ;;
      r )
        number_of_runs=$OPTARG
        ;;
      o )
        output_dir=$OPTARG
        ;;
      t )
        test_mode=1
        ;;  
      h )
        usage_exit
        ;;
      \? )
        echo "Invalid option: -$OPTARG"
        usage_exit
        ;;
      : )
        echo "Option -$OPTARG requires an argument."
        usage_exit
        ;;
      * )
        echo "Option -$OPTARG is not impemented"
        usage_exit
        ;;
    esac
  done

  if [[ -z $ci_software ]]; then
    usage_exit
  fi

  if [[ -z $branch ]]; then
    usage_exit
  fi

  if [[ -z $number_of_dry_runs ]]; then
    number_of_dry_runs=2
  fi

  if [[ -z $number_of_runs ]]; then
    number_of_runs=10
  fi
}

invalid_argument_exit() {
  echo "Invalid argument $1 $2"
  usage_exit
}

usage_exit() {
  echo "Usage: $0 -s <concourse|gitlab|jenkins> -b <master|ec1|ec2|ec3> [-d] [-r] [-o] [-t]"
  exit 1
}

start_docker() {
  $DOCKER_SCRIPT -s $ci_software
  sleep 60s
}

stop_docker() {
  $DOCKER_SCRIPT
}

start_dry_runs() {
  for i in `seq 1 $number_of_dry_runs`; do
    $CI_RUNNER -s $ci_software -b $branch -d
    sleep 15s
  done
}

start_runs() {
  for i in `seq 1 $number_of_runs`; do
    $CI_RUNNER -o $output_dir -s $ci_software -b $branch
    sleep 15s
  done
}

main $@