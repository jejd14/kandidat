#! /bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

main() {
    parse_arguments $@

    files="$(find $path -name $ci_software"_"$branch"*")"
    process "$files"
}

parse_arguments() {
  while getopts ":s:b:p:h" opt; do
    case $opt in
      s )
        case $OPTARG in
          concourse )
            ;&
          gitlab )
            ;&
          jenkins )
            ci_software=$OPTARG
            ;;
          * )
            invalid_argument_exit $opt $OPTARG
            ;;
        esac
        ;;
      b )
        case $OPTARG in
          master )
            ;&
          ec1a )
            ;&
          ec1b )
            ;&
          ec1c )
            ;&
          ec2a )
            ;&
          ec2b )
            ;&
          ec2c )
            ;&
          ec3a )
            ;&
          ec3b )
            ;&
          ec3c )
            branch=$OPTARG
            ;;
          * )
            invalid_argument_exit $opt $OPTARG
            ;;
        esac
        ;;
      p )
        path=$OPTARG
        ;; 
      h )
        usage_exit
        ;;
      \? )
        echo "Invalid option: -$OPTARG"
        usage_exit
        ;;
      : )
        echo "Option -$OPTARG requires an argument."
        usage_exit
        ;;
      * )
        echo "Option -$OPTARG is not impemented"
        usage_exit
        ;;
    esac
  done

  if [[ -z $ci_software ]]; then
    usage_exit
  fi

  if [[ -z $branch ]]; then
    usage_exit
  fi

  if [[ -z $path ]]; then
    path="."
  fi
}

usage_exit() {
  echo "Usage: $0 -s <concourse|gitlab|jenkins> -b <master|ec1|ec2|ec3> [-p]"
  exit 1
}


process() {
    while read file_name; do
        measurement_data="$($SCRIPT_DIR/ci_processor.bash $file_name)"
        echo "$measurement_data"
    done <<< "$1"
}

main $@