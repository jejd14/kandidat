#!/bin/bash

EXPERIMENTS_DIR=/opt/bachelor/experiments
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
MULTI_CI_RUNNER=$SCRIPT_DIR/multi_ci_runner.bash
DOCKER_SCRIPT=$SCRIPT_DIR/docker_script.bash

NUMBER_OF_DRY_RUNS=2
NUMBER_OF_RUNS=25

CI_SOFTWARE=(concourse jenkins gitlab)
EXPERIMENT_CONFIGURATIONS=(ec1a ec1b ec1c ec2a ec2b ec2c ec3a ec3b ec3c)

main() {
  initialize

  for software in ${CI_SOFTWARE[@]}; do
    start_docker $software

    for config in ${EXPERIMENT_CONFIGURATIONS[@]}; do
      start_ci $software $config
    done

    stop_docker $software
  done
}

initialize() {
  current_experiment_dir=$EXPERIMENTS_DIR/experiment_$(date '+%Y-%m-%dT%H:%M:%S:%N')
  mkdir --parents $current_experiment_dir
}

start_ci() {
  SOFTWARE=$1
  BRANCH=$2

  echo "--------------------------------------------"
  echo "Running $SOFTWARE with configuration $BRANCH"
  echo "--------------------------------------------"
  $MULTI_CI_RUNNER -s $SOFTWARE -b $BRANCH -d $NUMBER_OF_DRY_RUNS -r $NUMBER_OF_RUNS -o $current_experiment_dir -t
}

start_docker() {
  $DOCKER_SCRIPT -s $1
  sleep 120s

  if [[ "$1" == "concourse" ]]; then
    fly login --concourse-url http://localhost:4080 --target bachelor --username concourse --password changeme
    fly -t bachelor destroy-pipeline --non-interactive --pipeline bachelor-pipeline
    fly -t bachelor set-pipeline --non-interactive --pipeline bachelor-pipeline --config $SCRIPT_DIR/../concourse/pipeline.yml --var "private-repo-key=$(cat /opt/git-server/keys/id_rsa)"
    fly -t bachelor unpause-pipeline --pipeline bachelor-pipeline
  fi
}

stop_docker() {
  $DOCKER_SCRIPT
}

main
