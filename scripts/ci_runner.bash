#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CI_RUNNER_CONFIG_FILE=$SCRIPT_DIR/ci_runner.config
MEASURMENT_DIR="/opt/bachelor/measurements/"
COLLECTION_STATUS_DIR="/tmp/bachelor/collection_status/"
RAW_COLLECTION_FILE="/tmp/bachelor/raw_collection"
COLLECTION_FILE="/tmp/bachelor/collection"

is_dry_run=0

main() {
  ls -la /tmp/bachelor
  parse_arguments $@
  parse_config
  intialize
  collect_statistics

  echo "Start time: $stats_collection_start_time"
  echo "Stop time: $stats_collection_stop_time"
}

parse_arguments() {
  while getopts ":o:s:b:dh" opt; do
    case $opt in
      s )
        case $OPTARG in
          concourse )
            ;&
          gitlab )
            ;&
          jenkins )
            ci_software=$OPTARG
            ;;
          * )
            invalid_argument_exit $opt $OPTARG
            ;;
        esac
        ;;
      b )
        case $OPTARG in
          master )
            ;&
          ec1a )
            ;&
          ec1b )
            ;&
          ec1c )
            ;&
          ec2a )
            ;&
          ec2b )
            ;&
          ec2c )
            ;&
          ec3a )
            ;&
          ec3b )
            ;&
          ec3c )
            branch=$OPTARG
            ;;
          * )
            invalid_argument_exit $opt $OPTARG
            ;;
        esac
        ;;
      d )
        is_dry_run=1
        ;;
      o )
        MEASURMENT_DIR=$OPTARG
        ;;  
      h )
        usage_exit
        ;;
      \? )
        echo "Invalid option: -$OPTARG"
        usage_exit
        ;;
      : )
        echo "Option -$OPTARG requires an argument."
        usage_exit
        ;;
      * )
        echo "Option -$OPTARG is not impemented"
        usage_exit
        ;;
    esac
  done

  if [[ -z $ci_software ]]; then
    usage_exit
  fi

  if [[ -z $branch ]]; then
    usage_exit
  fi
}

invalid_argument_exit() {
  echo "Invalid argument $1 $2"
  usage_exit
}

usage_exit() {
  echo "Usage: $0 -s <concourse|gitlab|jenkins> -b <master|ec1|ec2|ec3> [-d] [-o]"
  exit 1
}

parse_config() {
  if [[ ! -f $CI_RUNNER_CONFIG_FILE ]]; then
    echo "Missing configuration file: $CI_RUNNER_CONFIG_FILE"
    exit
  fi

  source $CI_RUNNER_CONFIG_FILE

  if [[ -z $gitlab_token ]]; then
    missing_config_variables+=("gitlab_token")
  fi

  if [[ -z $gitlab_project_id ]]; then
    missing_config_variables+=("gitlab_project_id")
  fi

  if [[ -v missing_config_variables ]]; then
    invalid_config_exit
  fi
}

invalid_config_exit() {
  echo "Invalid configuration file: $CI_RUNNER_CONFIG_FILE"
  for missing_config_variable in ${missing_config_variables[@]}; do
    echo "Variable \"$missing_config_variable\" not set"
  done
  exit
}

intialize() {
  # Setup clean_up to be called on script exit
  trap clean_up EXIT

  mkdir --parents $MEASURMENT_DIR
  stats_collector
}

stats_collector() {
  $SCRIPT_DIR/stats_collector.bash $RAW_COLLECTION_FILE $COLLECTION_FILE &
  stats_collector_pid=$!
  echo "Hello $stats_collector_pid"
}

collect_statistics() {
  start_stats_collection
  stats_collection_start_time=$(date '+%Y-%m-%dT%H:%M:%S:%N')
  saved_collection_file="${MEASURMENT_DIR}/${ci_software}_${branch}_${stats_collection_start_time}"

  start_job

  inotifywait --event create $COLLECTION_STATUS_DIR |
    while read path action file; do
     echo "Trigger file created"
    done

  stats_collection_stop_time=$(date '+%Y-%m-%dT%H:%M:%S:%N')
  stop_stats_collection

  if [[ $is_dry_run == 0 ]]; then
    save_statistics
  fi
}

start_stats_collection() {
  rm --recursive --force $COLLECTION_STATUS_DIR/*

  docker stats > $RAW_COLLECTION_FILE &
  stats_collection_pid=$!
  echo "Hello $stats_collection_pid"
}

start_job() {
  case $ci_software in
    concourse )
      start_concourse
      ;;
    gitlab )
      start_gitlab
      ;;
    jenkins )
      start_jenkins
      ;;
  esac
}

start_concourse() {
  echo "Start concource test"
  fly login --target bachelor --username concourse --password changeme
  fly --target bachelor trigger-job --job bachelor-pipeline/$branch
}

start_gitlab() {
  echo "Start gitlab test"

  GITLAB_URL="http://localhost:5080/api/v4/projects/${gitlab_project_id}/trigger/pipeline?token=${gitlab_token}&ref=${branch}"

  echo $GITLAB_URL
  curl --request POST $GITLAB_URL
  # some logic to start a job in gitlab
}

start_jenkins() {
  echo "Start jenkins test"
  CRUMB=$(curl -s 'http://admin:adminadmin@localhost:3080/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)')
  echo $CRUMB
  curl -X POST -H "$CRUMB" http://admin:adminadmin@localhost:3080/job/project/job/$branch/build
}

stop_stats_collection() {
  kill $stats_collection_pid 2> /dev/null
  echo "Goodbye $stats_collection_pid"

  rm --recursive --force $COLLECTION_STATUS_DIR/*
}

save_statistics() {
  echo "Saving statistics in $saved_collection_file"
  sed -i '/HCONTAINER ID/c\\n' $COLLECTION_FILE
  mv $COLLECTION_FILE $saved_collection_file
}

clean_up() {
  echo "Start clean_up"
  stop_stats_collection
  kill $stats_collector_pid 2> /dev/null
  echo "Goodbye $stats_collector_pid"
  echo "Exit clean_up"
}

main $@
