#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

main() {
  parse_arguments $@
  
  stop_all

  if [[ $ci_software ]]; then
    start_$ci_software
  fi
}

parse_arguments() {
  while getopts ":s:akh" opt; do
    case $opt in
      s )
        case $OPTARG in
          concourse )
            ;&
          gitlab )
            ;&
          jenkins )
            ci_software=$OPTARG
            ;;
          * )
            invalid_argument_exit $opt $OPTARG
            ;;
        esac
        ;;
      a )
        start_all
        exit 1
        ;;
      k )
        stop_all
        exit 1
        ;;
      h )
        usage_exit
        ;;
      \? )
        echo "Invalid option: -$OPTARG"
        usage_exit
        ;;
      : )
        echo "Option -$OPTARG requires an argument."
        usage_exit
        ;;
      * )
        echo "Option -$OPTARG is not impemented"
        usage_exit
        ;;
    esac
  done
}

invalid_argument_exit() {
  echo "Invalid argument $1 $2"
  usage_exit
}

usage_exit() {
  echo "Usage: $0 [-s <concourse|gitlab|jenkins>]"
  exit 1
}

start_jenkins() {
  start jenkins
}

start_gitlab() {
  start gitlab
}

start_concourse() {
  start concourse
}

stop_all() {
  stop jenkins
  stop gitlab
  stop concourse
}

start_all() {
  start git-server
  start jenkins
  start gitlab
  start concourse
}

stop() {
  cd $SCRIPT_DIR/../$1
  docker-compose stop
}

start() {
  cd $SCRIPT_DIR/../$1
  docker-compose up -d
}

main $@
